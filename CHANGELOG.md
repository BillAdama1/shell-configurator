# Changelog

This is the notable changes of Shell Configurator Extension project.

## Version 5 - Whitesmoke (Major Release) - June 20 2022
The biggest major updates since 1 year suspended the development and rarely
release the newer version

### **Added:**
* **Metadata** - GNOME 41 and 42 support
* **Metadata** - libadwaita support
* **Library** - Main (General) Module
* **Library** - Looking Glass Module
* **Library** - Search Module
* **Library** - Notification Module
* **Library** - OSD Module
* **Library** - Show Panel on Overview configuration (Panel)
* **Library** - Auto-hide Panel configuration (Panel)
* **Library** - Panel elements visibility configuration (Panel)
* **Library** - Dash separator visibility configuration (Dash)
* **Library** - Dash Icon Size configuration (Dash)
* **Library** - Application Button configuration, visibility and position (Dash)
* **Library** - Static Background on Overview configuration (Overview)
* **Library** - Workspace background visibility configuration (Workspace)
* **Library** - Uniform workspace scale configuration (Workspace)
* **Library** - Workspace switcher static background configuration (Workspace)
* **Library** - Wraparound workspace switching configuration (Workspace)
* **Library** - Always show workspace switcher configuration (Workspace)
* **Library** - Workspace background border radius configuration (Workspace)
* **Library** - Type to search configuration (Search)
* **Library** - Double Super to App Grid configuration (App Grid)
* **Preferences** - Preset feature
* **Preferences** - Search feature
* **Preferences** - Suggested Extensions page
* **Extension** - Version codename, we use color name
* **Extension** - Translation files
* **Repo** - Contributing file
* **Repo** - Issues template file
### **Enhanced:**
* **Preferences** - Option settings binding instead of connecting signals
* **Library** - Using jsdoc for summary of the code
### **Changed:**
* **Library** - Separated components modules
* **Library** - Move search options from "Overview" configuration to "Search"
* **Library** - Separated configurations on overview module to individual module
* **Preferences** - Rewritten and redesigned look with search feature
### **Fixed:**
* **Extension** - App grid doesn't change persistently after restart GNOME Shell
* **Prefs** - Uneditable SpinButton
### **Removed:**
* **Metadata** - Older GNOME Versions (3.30, 3.32, 3.34) support
* **Script** - Extension Updater, the extension updater are already built-in
* **Library** - Enable hot corner configuration (Panel) - related to Older GNOME
  version support removal


## Version 4 (Technical Fixes) - May 9 2021
Refreshed from Version 2 and 3 with some fixes

### **Changed:**
* **Extension** - Fixed Overview restoring workspace switcher scale size that
when disabling extension makes destroying the Shell overview
* **Repo** - Review file


## Version 3 (Technical Fixes) - May 8 2021
Refreshed from Version 1 with some fixes

### **Changed:**
* **Extension** - Release state from system to user to make extension works when
installing on GNOME Shell Extension Website


## Version 2 (Technical Fixes) - May 8 2021
Refreshed from Version 1 with some fixes

### **Added:**
* **Extension** - GSettings schemas compiled binary


## Version 1 (Initial Release) - May 7 2021
Initial release of Shell Configurator

### **Added:**
* **Extension** - Extension Preferences
* **Library** - Top Panel Module
* **Library** - Dash Module
* **Library** - Overview Module
* **Library** - App Grid Module
* **Script** - Extension Installer (Build from Source Method)
* **Script** - Extension Updater
* **Metadata** - GNOME 3.36, 3.38, and 40 support
* **Metadata** - Older GNOME Versions (3.30 - 3.34) support
* **Repo** - Readme file
* **Repo** - References Documentation
* **Repo** - License file
* **Repo** - Changelog file
* **Repo** - To Do / Task file
* **Repo** - Versions Manager for updater