# Shell Configurator
### Configure and customize GNOME Shell with advanced settings.

![Shell Configurator Screenshot](./media/shell-configurator-screenshot.png)

## Purpose
It use for add, remove, configure, and customize GNOME Shell with advanced
settings. You can configure it like your own.

**You can configure:**
* Panel
* Dash (or Dock)
* Overview
* Workspace
* Search
* App Grid
* Looking Glass
* Notification
* OSD
* Screenshot UI (GNOME 42 above only)

For option references, see our [Wiki](https://gitlab.com/adeswantaTechs/shell-configurator/-/wikis/home)

## This extension is supported by the following versions
| GNOME Version                | State          | Supported Since     | Latest Version    | End Of Support |
| ---                          | ---            | ---                 | ---               | ---            |
| Older versions (3.30 - 3.34) | Unsupported*   | v1 - 7 May 2021     | v4 - 9 May 2021   | June 2022      |
| GNOME 3.36                   | Released       | v1 - 7 May 2021     | v5 - 20 June 2022 | June 2026      |
| GNOME 3.38                   | Released       | v1 - 7 May 2021     | v5 - 20 June 2022 | June 2026      |
| GNOME 40                     | Released       | v1 - 7 May 2021     | v5 - 20 June 2022 | June 2026      |
| GNOME 41                     | Released       | v5 - 20 June 2022   | v5 - 20 June 2022 | Q2 of 2027     |
| GNOME 42                     | Released       | v5 - 20 June 2022   | v5 - 20 June 2022 | Q2 of 2027     |

*\*We currently removed support for older versions. [More details here.](OLDER_VERSIONS_NOTE.md)*

* **Release state process: Under Construction &rarr; In development &rarr; In review &rarr; Released**
* **After GS Version support reaches the end of support date, the release state will be "Unsupported"**

## Installation
You can install this extension with these methods:

### Install from GNOME Shell Extensions website

Go to [GNOME Shell Extensions Website](https://extensions.gnome.org/extension/4254/shell-configurator/),
then install/enable by toggling switcher on the top right.

<a href="https://extensions.gnome.org/extension/4254/shell-configurator/">
    <img alt="Get it on extensions.gnome.org badge" height="120" src="./media/get-on-ego.png"></img>
</a>


### Install from GitLab Releases
Go to [Releases](https://gitlab.com/adeswantaTechs/shell-configurator/-/releases)
and download the extension package on the Packages section

then, follow these commands:
```bash
$ gnome-extensions install --force 'shell-configurator@adeswanta.shell-extension.zip'
```

### Build from Source method
Before installing, you need install these packages:
* `git` for cloning repository.
* `zip` for packing and unpacking archive.
* `gnome-shell-extensions` for packing and unpacking extension archive

then, follow these commands:
```bash
$ git clone https://gitlab.com/adeswantaTechs/shell-configurator.git
$ cd shell-configurator
$ ./installer.sh --build
# to install as user:
$ ./installer.sh --install USER
# or to install as system:
$ ./installer.sh --install SYSTEM
```

## Credits
This extension won't work better without these references:
* **[GNOME Shell source code on Gitlab](https://gitlab.gnome.org/GNOME/gnome-shell)** for shell library references
* **[GJS Guide Website](https://gjs.guide)** for porting extension **[for GNOME 40 (with GTK 4)](https://gjs.guide/extensions/upgrading/gnome-shell-40.html)**, **[for GNOME 42](https://gjs.guide/extensions/upgrading/gnome-shell-42.html)** and **[Documentation](https://gjs-docs.gnome.org/)**
* **[jsdoc](https://jsdoc.app/)** for javascript documenting guide

...and also without developers/creators:
* **[Just Perfection's GNOME Shell Desktop Extension](https://gitlab.gnome.org/jrahmatzadeh/just-perfection)** for API references and Prefs
* Edenhofer's **[Hide Workspace Thumbnails](https://extensions.gnome.org/extension/808/hide-workspace-thumbnails/)** for tweaking Workspace Thumbnails for GNOME 3.38 below from **[Minimalism-Gnome-Shell](https://github.com/Edenhofer/Minimalism-Gnome-Shell)**
* Thoma5's **[Bottom Panel](https://github.com/Thoma5/gnome-shell-extension-bottompanel)** for panel position, show on overview feature, and callbacks
* Selenium-H's **[App Grid Tweaks](https://github.com/Selenium-H/App-Grid-Tweaks)** for app grid configurations
* HROMANO's **[nohotcorner](https://github.com/HROMANO/nohotcorner/)** for disabling hot corners for GNOME Shell 3.32 below
* tuxor1337's **[Hide Top Bar](https://github.com/mlutfy/hidetopbar)** for auto hide top panel
* dz4k's **[Static background in overview](https://github.com/dz4k/gnome-static-background)** and Ralthuis's **[Vertical Overview](https://github.com/RensAlthuis/vertical-overview)** for static desktop background in overview
* theychx's **[Workspace Switch Wraparound](https://github.com/theychx/WorkspaceSwitcherWrapAround)** for wraparound workspace switching
* axxapy's **[Gnome 4x Overview UI Tune](https://github.com/axxapy/gnome-ui-tune)** for always show workspace switcher feature for GNOME 4x
* RaphaelRochet's **[Apps On Top](https://github.com/RaphaelRochet/apps-on-top)** for moving application button position feature

## License
**Shell Configurator is licensed under The GNU General Public License v3.0. See [LICENSE](https://gitlab.com/adeswantaTechs/shell-configurator/-/blob/master/LICENSE)**
