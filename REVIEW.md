# Shell Configurator Reviews

This is the review state for this extension to get release review state and the issues itself that the extension will be rejected.

## Version 5 - 20 June 2022
Review State: **Active**\
Issues: -

## Version 4 - 9 May 2021
Review State: **Active**\
Issues: -


## Version 3 - 8 May 2021
Review State: **Rejected**\
Issues:
* When disabling extension, it won't restore the workspace switcher scale size (MAX_THUMBNAIL_SCALE) that makes destroying the Shell overview:
![No Shell overview](https://i.imgur.com/SHbsZZJ.png)

* a lots of warnings on journalctl logs especially for Panel Position & Visibility and Preferences\
Logs:
```
> g_settings_set_value: value for key 'panel-height' in schema 'org.gnome.shell.extensions.shell-configurator' is outside of valid range.
> Trying to snapshot GtkImage 0x560da850d930 without a current allocation.
> Can't update stage views actor panelBox is on because it needs an allocation.
```


## Version 2 - 8 May 2021
Review State: **Rejected**\
Issues: We forgot change the install type on metadata.json


## Version 1 - 7 May 2021
Review State: **Rejected**\
Issues: We forgot compile the gresource schemas