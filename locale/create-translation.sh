#!/bin/bash

if [ ! $1 ]; then
    echo "[Shell Configurator] [ERROR] Language is required"
    exit 1
fi

# Change current directory to locale folder from this repo root
cd "$(git rev-parse --show-toplevel)/locale"

msginit -i template.pot --locale $1 -o $1.po